<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */

    /* protected $redirectTo = '/payment'; */


    protected function redirectTo()
{
  if (Input::get('submit') == 'monthly') {

     return  '/monthly';
   }
   elseif (Input::get('submit') == 'yearly') {
      return  '/yearly';


   }
}


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get the JSON long/lat.
     *
     * @return void
     */
     public function get_LongLat($address)
     {
        $address = str_replace(" ", "+", $address);
        $geocode = "https://maps.google.com/maps/api/geocode/json?address=" . $address;
        $url = file_get_contents($geocode);
        $lat_lng = get_object_vars(json_decode($url));

        $lat = $lat_lng['results'][0]->geometry->location->lat;
        $lng = $lat_lng['results'][0]->geometry->location->lng;

        $return_array['lat'] = $lat;
        $return_array['lng'] = $lng;

        return $return_array;
     }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'street' => 'required|string|max:255',
            'city' => 'required',
            'zip' => 'required',
            'state' => 'required',
            'affiliation' => 'required|string|max:255',
            'certification' => 'required|string|max:255',
            'phone' => 'required',
            'distance_served' => 'required',
            'bio' => 'required',
            'private_sessions' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $address = $data['street'] . '+' . $data['city'] . '+' . $data['zip'];

        $get_location = $this->get_LongLat($address);

        return User::create([
            'firstname' => $data['firstname'],
            'lastname' => $data['lastname'],
            'street' => $data['street'],
            'city' => $data['city'],
            'state' => $data['state'],
            'bio' => $data['bio'],
            'lng' => $get_location['lng'],
            'lat' => $get_location['lat'],
            'zip' => $data['zip'],
            'affiliation' => $data['affiliation'],
            'certification' => $data['certification'],
            'phone' => $data['phone'],
            'distance_served' => $data['distance_served'],
            'private_sessions' => $data['private_sessions'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
