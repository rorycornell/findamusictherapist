<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Http\Client\Curl\Client;
use Geocoder\Provider\BingMaps\BingMaps;
use Geocoder\Provider\Chain\Chain;
use Geocoder\Provider\FreeGeoIp\FreeGeoIp;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Request;


class LocationsController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index()
    {

        $zip = "";
        $users = DB::table('users')->get();
        return view('locations', ['users' => $users, 'zip' => $zip]);
    }
    
    public function send_to_locations(){
        $name = Request::all();
        $users = DB::table('users')->get();
        $zip = "";
        if($name){
            $zip = $name['zip'];
        }
        return view('locations', ['users' => $users, 'zip' => $zip]);
    }


}
