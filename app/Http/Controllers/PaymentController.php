<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Stripe\{Stripe, Subscription, Charge, Customer};

class PaymentController extends Controller
{
    public function store(){

      Stripe::setApiKey(config('services.stripe.secret'));

      $customer = Customer::create([

        'email' => request('stripeEmail'),

        'source' => request('stripeToken'),


      ]);


      Subscription::create([
  'customer' => $customer->id,
  "plan" => "1",
]);

return redirect('/edit');


    }

public function yearly(){

      Stripe::setApiKey(config('services.stripe.secret'));

      $customer = Customer::create([

        'email' => request('stripeEmail'),

        'source' => request('stripeToken'),


      ]);


      Subscription::create([
  'customer' => $customer->id,
  "plan" => "1",
]);

return redirect('/edit');


    }

public function monthly(){

      Stripe::setApiKey(config('services.stripe.secret'));

      $customer = Customer::create([

        'email' => request('stripeEmail'),

        'source' => request('stripeToken'),


      ]);


      Subscription::create([
  'customer' => $customer->id,
  "plan" => "2",
]);

return redirect('/edit');


    }
}
