<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Image;
use App\Http\Controllers\Controller;

class UserController extends Controller
{


  public function show($id)
   {
       return view('user.profile', ['user' => User::findOrFail($id)]);
   }

   public function edit()
    {
        return view('user.edit', array('user' => Auth::user()) );
    }

    public function update_avatar(Request $request){
    	// Handle the user upload of avatar
    	if($request->hasFile('avatar')){
    		$avatar = $request->file('avatar');
    		$filename = time() . '.' . $avatar->getClientOriginalExtension();
    		Image::make($avatar)->resize(300, 300)->save( public_path('/img/avatar/' . $filename ) );
    		$user = Auth::user();
    		$user->avatar = $filename;
    		$user->save();
    	}
      return view('user.edit', array('user' => Auth::user()) );
    }


    public function saveedits(Request $input){

      $user = User::find(Auth::user()->id);

      if ($input->has('bio'))
      $user->bio = $input['bio'];

      if ($input->has('firstname'))
      $user->firstname = $input['firstname'];

      if ($input->has('lastname'))
      $user->lastname = $input['lastname'];

      if ($input->has('street'))
      $user->street = $input['street'];

      if ($input->has('city'))
      $user->city = $input['city'];

      if ($input->has('state'))
      $user->state = $input['state'];

      if ($input->has('zip'))
      $user->zip = $input['zip'];

      if ($input->has('affiliation'))
      $user->affiliation = $input['affiliation'];

      if ($input->has('certification'))
      $user->certification = $input['certification'];

      if ($input->has('phone'))
      $user->phone = $input['phone'];

      if ($input->has('distance_served'))
      $user->distance_served = $input['distance_served'];

      if ($input->has('email'))
      $user->email = $input['email'];

      if ($input->has('password'))
      $user->password = bcrypt($input['password']);

    	$user->save();

      return view('user.edit', array('user' => Auth::user()) );



    }



}
