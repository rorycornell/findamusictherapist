<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('users', function(Blueprint $table)
      {
          $table->string('street');
          $table->string('city');
          $table->string('zip');
          $table->string('state');
          $table->string('lat');
          $table->string('lng');
          $table->string('affiliation');
          $table->string('certification');
          $table->string('phone');
          $table->string('distance_served');
          $table->string('private_sessions');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('users', function(Blueprint $table)
      {
          $table->dropColumn('street');
          $table->dropColumn('city');
          $table->dropColumn('zip');
          $table->string('state');
          $table->string('lat');
          $table->string('lng');
          $table->dropColumn('affiliation');
          $table->dropColumn('certification');
          $table->dropColumn('phone');
          $table->dropColumn('distance_served');
          $table->dropColumn('private_sessions');
        });
      }
    }
