@extends('layouts.app')

@section('content')

<style>
.btn{
	display:block;
	width:96%;
	max-width:10em;
	margin:1em auto;
	padding:.5em;
	font-size:1.25em;
	line-height:1;
	background-color:rgba(255,255,255,1);
	border-radius:2em;
	transition:.3s ease all;}

  </style>
<div class="main-content-wrapper">
  <div class="about-container">
    <header>
      <h2>Music Therapy:<br/>
      <span>Safe, Effective, Proven</span></h2>
    </header>
    <div>
      <p>Music therapy is the use of music to help people reach non-musical goals. Examples of these goals are: increasing social interaction among peers, increasing gross motor skills, and maintaining cognitive functioning. Goals depend largely on the client. For example, a young child with a physical impairment may need the help of a music therapist to improve his ability to reach above his head. The music therapist can create activities that specifically target this goal. The MT may be improvising a tune on the piano and asks the child to fill in empty spaces by playing the chime bells, which are placed above the child’s head. This encourages the client to reach above his head to play the chime bells. Music therapy is particularly effective because it disguises hard work with a fun activity. Reaching above his head without the music is probably tedious or painful for the client. However, the music serves as motivation or an incentive.</p><br/>
      <p>Music therapists work in a variety of settings with many different populations. You can often find music therapists working in hospitals, rehabilitation centers, schools, private practices, nursing homes, community centers, and psychiatric hospitals, among many other settings. Music therapists work with clients who have diagnoses such as Cerebral Palsy, Alzheimer’s disease, and Depression among others.</p><br/>
    </div>
  </div>
  <div class="about-associations-container">
    <div>
      <img class="img-about-large" src="img/about-link-association.png">
      <h2>Curious how music therapy is used in specific populations? Find out more from the American Music Therapy Association.</h2>
      <a class="btn" href="https://www.musictherapy.org" title="Go to MusicTherapy.org" target="new">Find Out More</a><br/>
    </div>
    <div class="about-journal-links">
      <h2>Journals and other publications that publish research on the effects of music therapy:</h2>
      <ul class="ul-berklee-links">
        <li><a href="https://www.voices.no" target="new" title="Visit Voices"><img src="img/about-link-voices.png"></a></li>
        <li><a href="https://academic.oup.com/jmt" target="new" title="Visit the Journal of Music Therapy"><img src="img/about-link-journal.png"></a></li>
        <li><a href="https://academic.oup.com/mtp" target="new" title="View Music Therapy Perspectives"><img src="img/about-link-perspectives.png"></a></li>
      </ul>
    </div>
  </div>
  <!--<div class="about-berklee-container">
    <div>
      <img class="img-about-large" src="img/about-link-berklee.png">
      <ul>
        <li><a href="http://www.musictherapytales.com/" title="Visit Music Therapy Tales" target="new">To learn about music therapy through the clinical experiences of practiced music therapists</a></li>
        <li><a href="https://www.musictherapy.org/" title="Visit MusicTherapy.org" target="new">If you have more questions, you can turn to the American Music Therapy Association</a></li>
        <li><a href="https://www.berklee.edu/music-therapy" title="Visit Berklee.edu" target="new">Berklee College of Music has both undergraduate and graduate degree programs for music therapy. To learn more about their programs</a></li>
      </ul>
    </div>
    <div>
      <p>The Berklee Music Therapy Club seeks to provide all students with resources about music therapy to increase understanding. Events held by the Berklee MT club are often open to the public and advertised through the club’s social media accounts.</p>
      <ul>
        <li><a href="https://www.facebook.com/groups/163858153728364/" target="new">Facebook</a></li>
        <li><a href="https://www.instagram.com/berkleemthclub/" target="new">Instagram</a></li>
        <li><a href="mail-to:musictherapyclub@berklee.edu" target="new">Email</a></li>
      </ul>
    </div>-->
  </div>
  <!--<div class="about-associations-container">
    <h2>Accreditation</h2>
    <p>Curabitur blandit tempus porttitor. Nullam quis risus eget urna mollis ornare vel eu leo. Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>
    <img src="img/accreditation-1.png"><img src="img/accreditation-1.png"><img src="img/accreditation-1.png">
  </div>-->
</div>


@endsection
