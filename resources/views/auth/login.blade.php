@extends('layouts.app')

@section('content')
<div class="main-content-wrapper">

                    <form class="tr-container" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <h2>Login</h2>
                        <fieldset>
                          <h3>Account Information</h3>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">E-Mail Address</label><input id="email" type="email" placeholder="Enter Email Address Here..." class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <label for="password">Password</label><input id="password" type="password" class="form-control inputpass" name="password" required>

                          @if ($errors->has('password'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                      </div>

                      <div class="form-group">

                                <button class="tr-btn" type="submit">Submit</button>
                          </div>

                    </form>
                </div>

@endsection
