@extends('layouts.app')

@section('content')
<div class="main-content-wrapper">

                    <form class="tr-container" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <h2>Register as a Music Therapist</h2>
                        <fieldset>
                          <h3>Making it easy to discover Music Therapists</h3>
                          Our mission is to make it easy for people to discover Certified Music Therapists in their area. Many people have yet to discover Music Therapy, and by listing yourself on www.FindaMusicTherapist.com we will make sure the people looking for you, can find you. Our current database is limited to Music Therapists with CBMT and CAMT Certifications</a>

                        </fieldset>
                        <fieldset>

                          <h3>Account Information</h3>

                          <div class="form-group">
                          <label for="email">E-Mail Address</label><input id="email" type="email" placeholder="Enter Email Address Here..." class="form-control" name="email" value="{{ old('email') }}" required>

                              </div>


                        <div class="form-group">
                            <label for="password">Password</label><input id="password" type="password" class="form-control inputpass" name="password" required>


                        </div>
                          <div class="form-group">
                              <label for="password-confirm" class="o-border-bottom">Confirm Password</label><input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                          </div>
                        </fieldset>
                        <fieldset>
                          <h3>Personal Information</h3>
                            <div class="form-group">
                          <label>First Name</label><input id="firstname" type="text" placeholder="Enter First Name Here.." class="form-control" name="firstname" value="{{ old('firstname') }}" required autofocus>

                          </div>
                          <div class="form-group">
                          <label>Last Name</label><input required="" type="text" placeholder="Enter Last Name Here.." class="form-control" name="lastname" id="lastname" value="{{ old('lastname') }}" required autofocus>

                        </div>
                          <div class="form-group">
                        <label>Bio</label><input required="" type="text" placeholder="Enter a Bio here.." class="form-control" name="bio" id="bio" value="{{ old('bio') }}" required autofocus>

                      </div>
                            <div class="form-group">
                          <label>Address</label><input type="text" placeholder="Enter Street Address Here.." class="form-control" name="street" id=street value="{{ old('street') }}" required autofocus>

                        </div>

                        <div class="form-group">
                        <label>City</label><input type="text" placeholder="Enter City Here.." class="form-control" name="city" id=city value="{{ old('city') }}" required autofocus>

                        </div>
                          <div class="form-group">
                        <label>State</label><input type="text" placeholder="Enter State Here.." class="form-control" name="state" id=state value="{{ old('state') }}" required autofocus>

                        </div>

                          <div class="form-group">
                        <label>Zip</label><input type="text" placeholder="Enter Zip Code Here.." class="form-control" name="zip" id=zip value="{{ old('zip') }}" required autofocus>

                        </div>
                        </fieldset>
                        <fieldset>
                          <h3>Practice Information</h3>

            <div class="form-group">
                          <label>Affiliation</label><input type="text" placeholder="Enter Affiliation Here.." class="form-control" name="affiliation" id=affiliation value="{{ old('affiliation') }}" required autofocus>

                          </div>

                            <div class="form-group">
                          <label>Certification</label><input type="text" placeholder="Enter Certification Here.." class="form-control" name="certification" id=certification value="{{ old('certification') }}" required autofocus>

                          </div>

          <div class="form-group">
                          <label>Phone</label><input type="text" placeholder="Enter Phone Number Here.." class="form-control" name="phone" id=phone value="{{ old('phone') }}" required autofocus>

                          </div>

                            <div class="form-group">
                          <label>Distance Served</label><input type="text" placeholder="In miles..." class="form-control" name="distance_served" id=distance_served value="{{ old('distance_served') }}" required autofocus>
                        
                          </div>
                          <label class="no-border-bottom">Do you offer Private Sessions?</label><input type="text" id="private_sessions" data-toggle="toggle" data-on="1" data-off="0" name="private_sessions">
                        </fieldset>

                        <fieldset>
                          <h3>Billing Options</h3>

                        <div class="form-group">




                                  <button class="btn btn-secondary" type="submit" id="submit" name="submit" value="monthly">$4.99/month</button>
                                  <button class="btn btn-secondary" type="submit" id="pay" name="submit" value="yearly">$39.99/year 33% off</button>


                            </div>
                          </fieldset>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
