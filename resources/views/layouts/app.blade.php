<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- TITLE -->
		<title>Find A Music Therapist</title>

		<!-- META TAGS -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<meta name="description" content="Find a music therapist in your area.">
		<meta name="keywords" content="Find, Music, Therapist, Point, Motion, Therapy, Search, Near, Me">
	    <meta name="author" content="Point Motion">
		<meta name="copyright" content="Point Motion 2017"/>

		<!-- SCRIPTS FOR LOAD -->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->

	    <!-- ANALYTICS -->

	    <!-- FONTS -->
	    <link href="" rel="stylesheet">

		<!-- STYLES -->



		 <link href="{{ secure_asset('/css/styles.css') }}" rel="stylesheet">



		<!-- FAVICONS -->
		<link rel="apple-touch-icon" sizes="180x180" href="ico/">
		<link rel="icon" type="image/png" sizes="32x32" href="ico/">
		<link rel="icon" type="image/png" sizes="194x194" href="ico/">
		<link rel="icon" type="image/png" sizes="192x192" href="ico/">
		<link rel="icon" type="image/png" sizes="16x16" href="ico/">
		<link rel="manifest" href="ico/">
		<link rel="mask-icon" href="ico/" color="#000000">
		<link rel="shortcut icon" href="ico/">
		<meta name="apple-mobile-web-app-title" content="ico/">
		<meta name="application-name" content="ico/">
		<meta name="msapplication-TileColor" content="ico/">
		<meta name="msapplication-TileImage" content="ico/">
		<meta name="msapplication-config" content="ico/">
		<meta name="theme-color" content="#000000">

		<!-- OPEN GRAPH -->
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="http://www.findamusictherapist.com"/>
		<meta property="og:description" content=""/>
		<meta property="og:title" content="Point Motion | Find A Music Therapist"/>
		<meta property="og:image" content="ico/"/>

		<!-- TWITTER -->
		<meta name="twitter:card" content=""/> <!-- Card Types: “summary”, “summary_large_image”, “app”, or “player” -->
		<meta name="twitter:site" content="@"/> <!-- Twitter Account Associated with Site -->
		<meta name="twitter:creator" content="@"/> <!-- Twitter Account Associated with Creator -->

		<meta name="_token" content="{!! csrf_token() !!}"/>
	</head>

	<body>
		<!-- GLOBAL HEADER -->
		<header class="global-header" title="">
			<div class="gh-logo">
				<a class="gh-toggle" href="javascript:;" title="Show Menu"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200"><polygon points="200 72.63 127.37 72.63 127.37 0 72.63 0 72.63 72.63 0 72.63 0 127.37 72.63 127.37 72.63 200 127.37 200 127.37 127.37 200 127.37 200 72.63"/></polygon></svg></a><!--<img src="img/logo.png" alt="Point Motion | Find A Music Therapist">--><a href="{{URL::to('/')}}" title="Point Motion | Find A Music Therapist Home"><h1>Find A Music Therapist</h1></a>
			</div>
			<!-- GLOBAL HEADER NAV -->
			<nav class="gh-nav">
				<ul>
					<li class="gh-nav-search"><a href="{{ url('/search') }}">Search Therapists</a></li>
          <li class="gh-nav-about"><a href="{{ url('/about') }}">About</a></li>
				@if (Auth::guest())

					<li class="gh-nav-login"><a class="gh-nav-login-link"  data-trigger="focus" href="/login">Login</a></li>
          <li class="gh-nav-get-listed"><a href="{{ url('/register') }}">Get Listed</a></li>
          @else

          <li class="gh-nav-login"><a href="{{ route('logout') }}">Logout {{ Auth::user()->firstname }}

          </a>
          @endif


          </ul>

			</nav>
		</header>
		<script>
			$(document).ready(function(){
				$('.gh-toggle').click(function(){
					$(this).closest('.global-header').find('.gh-nav').toggleClass('gh-nav-open');
					$(this).toggleClass('gh-logo-open');
					});
				$('.gh-nav-login-link').click(function(){
					$(this).closest('.global-header').find('.gh-nav-login-popover').toggleClass('gh-nav-login-popover-open');
					});
				$('.gh-nav-login-popover-dismiss').click(function(){
					$(this).closest('.global-header').find('.gh-nav-login-popover').toggleClass('gh-nav-login-popover-open');
					});
			});
		</script>


			@yield('content')






		<!-- PAGE CONTENT -->
		<div class="main-content-wrapper">
		</div>
		<!-- PAGE FOOTER -->
		<footer>
			<p>Copyright 2017 Point Motion</p>
			<p>info@findamusictherapist.com</p>
		</footer>
		<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/3897235.js"></script>
<!-- End of HubSpot Embed Code -->
	</body>
</html>
