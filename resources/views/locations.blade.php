@extends('layouts.app')

@section('content')

<style media="screen">
html, body, #map-canvas {
       height: 100%;
       margin: 0px;
       padding: 0px
     }
     .controls {
       margin-top: 16px;
       margin-left: 16px;
       border: 1px solid transparent;
       border-radius: 2px 0 0 2px;
       box-sizing: border-box;
       -moz-box-sizing: border-box;
       height: 32px;
       outline: none;
       box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
     }

     #pac-input {
       background-color: #fff;
       font-family: Roboto;
       font-size: 15px;
       font-weight: 300;
       margin-left: 12px;
       padding: 0 11px 0 13px;
       text-overflow: ellipsis;
       width: 400px;
     }

     #pac-input:focus {
       border-color: #4d90fe;
     }

     .pac-container {
       font-family: Roboto;
     }

     #type-selector {
       color: #fff;
       background-color: #4d90fe;
       padding: 5px 11px 0px 11px;
     }

     #type-selector label {
       font-family: Roboto;
       font-size: 13px;
       font-weight: 300;
     }


</style>

  <!-- PAGE CONTENT -->
  <div class="main-content-wrapper">
    <div class="sfamt-container">
      <p>Enter your Zip Code</p>


    </div>

                      <div class="sfamt-map" id="map" > </div>
                      <div><select id="locationSelect" style="visibility: hidden"></select></div>
  </div>

<input id="addressInput" class="controls" type="text" placeholder="
Note: this directory contains information as reported and entered by individuals. It is not intended to be official verification for MT-BC, training or education status. Please inquire of the Certification Board for Music Therapists (www.cbmt.org) for verification of current MT-BC status and contact the National Music Therapy Registry (301) 562-9330 for verification of RMT/CMT/ACMT status.  Information herein MAY NOT be used in any way for advertising, research, telemarketing or other business purposes." value="{{ $zip }}">



  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCx8206Ee_bsX1rui8EGAUf36vm-YYmuS8&libraries=places&callback=initMap"></script>
<script type='text/javascript'>
          function initMap() {



            var latlng = new google.maps.LatLng(42.13580820000001, -71.48918859999999); // default location



            var myOptions = {
                zoom: 10,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };

            var map = new google.maps.Map(document.getElementById('map'),myOptions);


            var markers = {!!json_encode($users)!!}


              var input = document.getElementById('addressInput');

              var searchBox = new google.maps.places.SearchBox(input);

              itemsloaded    =  google.maps.event
                          .addDomListener(document.body,
                                          'DOMNodeInserted',
                                          function(e){
                            if(e.target.className==='pac-item'){
                              google.maps.event.removeListener(itemsloaded);
                              google.maps.event.trigger( input, 'keydown', {keyCode:40})
                              google.maps.event.trigger( input, 'keydown', {keyCode:13})
                              google.maps.event.trigger( input, 'focus')
                              google.maps.event.trigger( input, 'keydown', {keyCode:13})
                            }
                          });










              $.each(markers, function(i, point) {
                var url = '<a href="/user/' + point.id + '">Profile</a>'
                var name = '<a href="/user/' + point.id + '">' + point.firstname + ' ' + point.lastname + '</a>'
                var contentString = ('<h1>' + name +  '</h1>'+ '<p>' + point.street + '</p> '+ point.city + ', '+ point.state + ' ' + point.zip +
                '<p>' + url +'</p>')
                var infowindow = new google.maps.InfoWindow({
                  content: contentString
                });

                  marker = new google.maps.Marker({
                      position: new google.maps.LatLng(point.lat, point.lng),
                      name:name,
                      map: map,

                  });
                  google.maps.event.addListener( marker, 'click', function(e){

                      infowindow.open( map, this );
                  }.bind( marker ) );

              });

              // Create the search box and link it to the UI element.

    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

              // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });

    google.maps.event.addDomListener(window, 'load', initialize);
          }
      </script>

@endsection
