<!DOCTYPE html>
<html>
  <head>
    <style>
      #map {
        height: 400px;
        width: 100%;
       }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  </head>
  <body>
    <h3>Google Maps Demo</h3>
    <div id="map"></div>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCx8206Ee_bsX1rui8EGAUf36vm-YYmuS8&libraries=places&callback=initMap"></script>
  <script type='text/javascript'>


        function initMap() {

          var markers = {!!json_encode($users)!!}

            var latlng = new google.maps.LatLng(42.13580820000001, -71.48918859999999); // default location

            var myOptions = {
                zoom: 10,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };

            var map = new google.maps.Map(document.getElementById('map'),myOptions);
            var infowindow = new google.maps.InfoWindow(), marker, lat, lng;

            $.each(markers, function(i, point) {

                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(point.lat, point.lng),
                    name:name,
                    map: map
                });
                google.maps.event.addListener( marker, 'click', function(e){
                    infowindow.setContent( this.name );
                    infowindow.open( map, this );
                }.bind( marker ) );

            });
        }
    </script>
  </body>
</html>
