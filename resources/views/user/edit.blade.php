
@extends('layouts.app')

@section('content')

<style>
input[type="file"] {
  line-height: 2px;
  border: 100px;
}

div.form
{
    display: block;
    text-align: center;
}



</style>

<div class="main-content-wrapper">


  <div class="therapist-bio">
    <div class="bio-info">


        <div class="headshot">
          <img src="/img/avatar/{{ $user->avatar }}" title="">
        </div>




<div class="form">


          <form enctype="multipart/form-data" action="/editpic" method="POST">
            <label>Update Profile Image</label>
            <br>
            <input type="file" class="file" name="avatar">
            <br>
            <br>
            <input type="submit" class="pull-right btn btn-sm btn-primary">
          </form>
</div>


        <label>Bio</label>
            <form method="POST" action="{{route('edit')}}">
        <textarea rows="6" name="bio" placeholder="">{{$user->bio}}</textarea>
      </header>
      <h5>Contact</h5>
      <label>Certification: {{$user->certification}}</label>
      <label>Affiliation: {{$user->affiliation}}</label>
      <label>Phone Number</label><input type="text" placeholder= "{{$user->phone}}" class="form-control" value = "" name="phone">


      <h5>Details</h5>
      <label>Distance Served (in Miles)</label><input type="text" value="{{$user->distance_served}}" class="form-control" name="distance_served">

    </div>
    <div class="bio-map">
      <button type="submit" class="profile-edit-finish">Save Changes</button>


    </div>
    </form>
  </div>

@endsection
