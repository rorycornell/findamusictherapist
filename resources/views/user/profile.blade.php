


@extends('layouts.app')

@section('content')

<div class="main-content-wrapper">
  <div class="therapist-bio">
    <div class="bio-info">
      <header>
        <div class="headshot">
          <img src="/img/avatar/{{ $user->avatar }}" title="">
        </div>
        <h3>{{$user->firstname . ' ' . $user->lastname}}</h3>
        <h4>{{$user->bio}}</h4>
      <h5>Contact</h5>
      <p><span>Address</span>{{$user->street}}<br/>
      <span class="bio-adress-line-2"></span>{{$user->city . ' ' . $user->zip}} </p>
      <p><span>Email</span>{{$user->email}}</p>
      <p><span>Phone</span>{{$user->phone}}</p>
      <h5>Details</h5>
      <p>{{$user->firstname . ' ' . $user->lastname}} serves patients within a <strong>{{$user->distance_served}} mile</strong> radius.</p>
      <p>Private Sessions: {{$user->private_sessions}}</p>
    </div>
    <iframe class="bio-map" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAUl_fk5GscSUEguTS7V9RWh9309pG2wtI&q={{$user->city}},+{{$user->state}}/" allowfullscreen></iframe>
  </div>
</div>

@endsection
