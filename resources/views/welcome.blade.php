@extends('layouts.app')

@section('content')

<!-- PAGE CONTENT -->

<div class="main-content-wrapper">
  <div class="famt-container">
    {!! Form::open(['action' => 'LocationsController@send_to_locations'], null, ['id' => 'FindAMusicTherapist']) !!}
      <h2 style="color:black;">Find a Music Therapist In Your Area</h2>
      <p style="color:black;">Enter your zip code below to search your area.</p>
      <fieldset>
      {!! Form::text('zip', '', array('placeholder'=>'Zip', 'maxlength' => 5)) !!}
      {!! Form::submit('Search') !!}
      </fieldset>
    {!! Form::close() !!}
  </div>
  <div class="hpow-container">
    <div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 300"><style>.cls-1{fill:#c85f00;}.cls-2{fill:#fdfdfd;}</style><path class="cls-1" d="M134.49,176c-.32-1.7-1.74-2.36-3-3-1.87-1-3.83-1.75-5.78-2.53a63.66,63.66,0,0,1-14-7.5c-1.06-.76-1.59-.27-2.19.44-1.58,1.87-3.17,3.72-4.72,5.62-3.71,4.53-8.65,5.2-13.86,4-10.42-2.45-18.76-13.24-18.86-24,0-4.25,1.19-6.14,5.29-7.38,2.4-.72,4.35-2.18,6.49-3.32a1.65,1.65,0,0,0,.89-2q-1.63-8.19-3.2-16.39c-.18-.92-.62-1.28-1.52-1.21a10.66,10.66,0,0,0-2,.12c-4.69,1.35-9.4.7-14.13.28-3.64-.32-4.76-3.07-5.67-5.88-2.77-8.58-3.3-17.21-.28-25.87.13-.38.28-.77.44-1.14,2.75-6.42,5.49-8,12.41-7,2.77.39,5.57.19,8.34.46,1,.1,1.45-.36,1.74-1.26C82.21,74.19,83.63,70,85,65.81a2.64,2.64,0,0,0-.9-3.12c-3-2.18-6.06-4.39-9.65-5.54-4.19-1.34-5.49-3.19-5.61-7.6-.26-9.6,8.4-21.46,18.63-23.78,5.21-1.18,10-.61,14,3.75,3.73,4.15,3.86,4.05,8.33,1a49.5,49.5,0,0,1,17.86-7.66c3.15-.66,3.19-.68,3.23-3.78.05-3.46,0-6.93,0-10.39S131.78,4,134.93,3C142.3.48,149.82-.75,157.58.49a16.3,16.3,0,0,1,7.63,3.2c4.06,3.18,4.76,7.69,4.88,12.42,0,.13,0,.27,0,.41,0,1.83-.54,3.85.06,5.45s3,1.26,4.61,1.94a75.14,75.14,0,0,1,16.62,9.83c1.89,1.48,3.29,1.45,5,.13a29.07,29.07,0,0,1,11.08-4.94,7.37,7.37,0,0,1,6,.89,46.38,46.38,0,0,1,10.66,8.78c3.61,4.25,4.71,9.42,3.86,14.91-.44,2.89-2.29,4.81-4.87,6.11-4.69,2.37-4.72,2.41-2.22,7A33.45,33.45,0,0,1,224.66,78c.25,1.72,1,2.36,2.68,2,.8-.15,1.64-.1,2.42-.31,5.76-1.58,10.24.64,14.46,4.41,4.58,4.1,5.23,9.47,5.57,15A36.23,36.23,0,0,1,248.3,111a11.41,11.41,0,0,1-12.94,8.3c-3.43-.56-6.83-1.12-10.3-.15a3.08,3.08,0,0,0-2.45,2.56q-1.28,5.35-2.93,10.6a2.61,2.61,0,0,0,.92,3.18,17.27,17.27,0,0,0,4.45,2.81c5.54,2.38,7.46,5.65,6.17,13.38a18.08,18.08,0,0,1-7.47,12c-2.19,1.6-4.17,3.48-6.47,5a8.24,8.24,0,0,1-7,1.1A29.47,29.47,0,0,1,199.79,165c-.22-.16-.46-.3-.68-.45-3-2-5.71-2.56-8.72.26a21.25,21.25,0,0,1-9,4.64,30.2,30.2,0,0,0-4,1.55c-4.44,2-4.43,2-4.14,6.85a28.09,28.09,0,0,0,.08,4.67c.12,3.88-.5,7.47-4.17,9.74A16.08,16.08,0,0,1,163,194.6c-2.15.3-4.33.41-6.45.82a24.83,24.83,0,0,1-12.41-.94c-5.31-1.68-8.59-4.64-8.2-10.87C136.07,181.15,136,178.33,134.49,176Zm45.35-86.23a191.66,191.66,0,0,0-1.29-24.21c-.24-2.24-1.51-3.13-3.64-2.49-2.65.8-5.28,1.7-7.9,2.62-12.14,4.26-23.9,9.43-35.6,14.75-2.92,1.33-3.94,3.46-4,6.62-.06,9.23.23,18.47-.4,27.69-.06.89.24,2-1.14,2.26-2.15.46-4,1.64-6,2.43-5.12,2-7.88,6-9.51,11-1.15,3.51.89,6.27,4.61,6.27a15.68,15.68,0,0,0,9.36-3c2.48-1.78,5-3.56,7.33-5.51,2.62-2.18,5.16-4.48,5-8.4-.08-1.48.1-3,.14-4.46.16-6.72.88-13.43.65-20.15,0-1.25.48-1.6,1.44-2q14.44-6.52,28.84-13.14c2.6-1.2,2.93-1.06,3.13,1.89a125.42,125.42,0,0,1,0,14.87c-.12,2.33-.37,4.11-3.29,4a3,3,0,0,0-1.33.47c-5.86,2.93-10.44,7-12.61,13.45-1,2.9-.23,4.55,2.79,5.32a13.57,13.57,0,0,0,8-.62c4.16-1.46,7.69-4,11.24-6.52a9.2,9.2,0,0,0,4.27-8.44C179.69,99.56,179.84,94.66,179.84,89.77Z"/></path><path class="cls-2" d="M134.49,176c1.55,2.33,1.58,5.15,1.43,7.62-.39,6.23,2.89,9.18,8.2,10.87a24.83,24.83,0,0,0,12.41.94c2.12-.41,4.3-.52,6.45-.82a16.08,16.08,0,0,0,6.23-2.35c3.67-2.26,4.3-5.85,4.17-9.74a28.09,28.09,0,0,1-.08-4.67c20.49,5.37,39.3,13.82,54.77,29.66a94.06,94.06,0,0,1-19.21-.71c-11.57-1.59-23-4-34.53-5.73-4.35-.65-8.55-2.16-13-2.63-9-1-17,.88-23.61,7.42-.24.24-.5.46-.73.71a3.69,3.69,0,0,0-.33.49,1.6,1.6,0,0,0,1.38,0,58.4,58.4,0,0,1,30.09-2,398.12,398.12,0,0,0,51.75,6.88c7.33.5,14.54,1.22,21.59,3.21,4.14,1.17,8.18,2.65,12.25,4.06,4.72,1.64,9.51,3.11,14,5.4,6.22,3.19,11,8.05,15.67,13.14,2.89,3.17,5.41,6.43,5.31,11l0,0c-.75.5-1.33-.11-1.85-.41-11.54-6.54-24.11-8.41-37.13-8.18-2.65,0-5.3.09-7.94.3-2.07.17-3.74-.82-5.33-1.74C231,235.51,225,234.32,219,232.85c-8.43-2.08-17-1.67-25.55-1.33a2.56,2.56,0,0,1-3-2.08,53.6,53.6,0,0,0-5-10.24,4.11,4.11,0,0,0-.44-.69c-.58-.64-1.16-1.68-2.1-1.31-1.14.46-.45,1.61-.24,2.35,1.3,4.5,3,8.88,4.31,13.37a49.74,49.74,0,0,1,1.34,22.77c-.17,1-.55,2-.79,2.94-.45,1.9.32,3.58,1.89,4,1.88.47,2.88-.5,3.53-2.19a21.19,21.19,0,0,0,1.38-7.37c0-1.83.78-2,2.2-1.67,8,1.92,16.19,2.89,23.69,6.41,6.5,3.06,13.59,3.74,20.47,5.23,11.38,2.46,22.18,6.32,32,12.6,2.7,1.72,2.66,1.82.18,3.86a62.61,62.61,0,0,1-19.35,10.66c-.77.27-1.42.39-2-.29-3.66-4-8.68-5.83-13.47-7.91a141.43,141.43,0,0,0-20.84-7,54.53,54.53,0,0,0-15.74-1.85c-6.83.26-12.28-2.22-17.53-6.29A97.13,97.13,0,0,0,162.7,254c-6.09-2.6-12.32-4.46-19.05-4.52-2.43,0-4.89-.13-7.34-.19a4.72,4.72,0,0,0-2.54.53,49.89,49.89,0,0,1,11.93,1.78,112.82,112.82,0,0,1,18.13,7.19c8.38,4,15.68,9.7,23.25,15a12.25,12.25,0,0,0,6.2,2.09c11.08,1,22.22,1.87,32.63,6.43,4.27,1.87,8.9,2.88,13,5.23a38.18,38.18,0,0,1,7,4.54c-2.21,1.23-4.54,1.18-6.73,1.6-5.18,1-10.37,2.34-15.6,2.64-6.9.4-13.82.85-20.74,1.07-16.29.52-32.49-.08-48.42-3.75-16.09-3.71-32-8.2-47.2-14.66-6.37-2.7-11.11-7.48-15.76-12.33C86.6,261.6,82.1,256.24,77.7,250.8c-.35-.43-.68-1.05-1.27-.71s0,.94.15,1.39c1.41,5.5,4.6,9.92,8.37,14,4.69,5.1,9.32,10.25,15.29,14.37-4.31,3.14-8.42,6.21-12.61,9.15a122.12,122.12,0,0,1-16.73,9.65c-4.36,2.13-8.67,1.49-13-.44A62.09,62.09,0,0,1,40.59,286.6C32.89,279.32,25.25,272,19,263.31A55.33,55.33,0,0,1,8.31,235a24.8,24.8,0,0,1,2.42-13.06,14.66,14.66,0,0,1,6.32-6.43c6.19-3.42,13-4.81,19.85-5.77,16.18-2.25,31.56-7.12,46.57-13.39,5.51-2.3,10.42-5.73,15.8-8.26C110.56,182.73,121.95,177.68,134.49,176Z"/></path><path class="cls-2" d="M201.47,235.72c9.12-.32,17.94,1.66,26.75,3.6,3.61.8,6.77,2.77,10.12,4.24a13.83,13.83,0,0,0,6.31,1.23c6.85-.3,13.7-.6,20.45,1.13a133.25,133.25,0,0,1,21,7.14,2.91,2.91,0,0,0,.95.29c1.65.11,1.74,1.09,1.49,2.41-.85,4.44-3.14,8.21-5.49,11.95-1.08,1.72-2.42,3.28-3.5,5-.73,1.16-1.57,1.25-2.42.41a23.6,23.6,0,0,0-7.94-4.8c-5.27-2.16-10.54-4.35-15.93-6.19a129.86,129.86,0,0,0-24.54-5.71,9.54,9.54,0,0,1-2.53-.73A140,140,0,0,0,196,246.57c-1.42-.26-1.94-.84-2-2.22a33.77,33.77,0,0,0-1.24-7c-.32-1.16,0-1.58,1.18-1.63A36.49,36.49,0,0,1,201.47,235.72Z"/></path><path class="cls-2" d="M288.73,248.71a1.1,1.1,0,0,1,0,.18s0,.05-.07.08l.12-.28Z"/></path></svg>
      <header>
        <h2>The Healing Power of Music</h2>
        <p>We all intuitively know the power that music can have to heal. From getting us through a tough day to powering through a hard work-out, music's power to heal is all around us. Music therapists use years of practice and training to help you get the most out of music, to support clinical, health and wellness goals that are important to you. <br/><br/>

        <a class="a-button" href="{{ url('about') }}">Learn More ></a></p>
      </header>
    </div>
  </div>
  <div class="featt-container">
    <h2>Featured Music Therapist</h2>
    <a class="featured-therapist" href="https://findamusictherapist.com/user/1" title="See more about this therapist">
      <header>
        <div class="headshot">
          <img src="/img/avatar/{{ $user->avatar }}" title="">
        </div>
        <h3>Kate St. John</h3>
        <h4>NeuroRhythm Music Therapy</h4>
      </header>
    </a>
    <!--<a class="featured-therapist" href="{{ url('/therapist/wendycarlos') }}" title="See more about this therapist">
      <div class="headshot">
        <img src="{{ URL::to('img/headshot-wendycarlos.jpg') }}" title="">
      </div>
      <header>
        <h3>Dr. Wendy Carlos</h3>
        <h4>Trained Music Therapist and Hero</h4>
      </header>
    </a>
    <a class="featured-therapist" href="{{ url('/therapist/laurieanderson') }}" title="See more about this therapist">
      <div class="headshot">
        <img src="{{ URL::to('img/headshot-laurieanderson.jpg') }}" title="">
      </div>
      <header>
        <h3>Dr. Laurie Anderson</h3>
        <h4>Trained Music Therapist and Hero</h4>
      </header>
    </a>-->
  </div>
</div>


@endsection
