@extends('layouts.app')

@section('content')
<div class="main-content-wrapper">
  <div class="therapist-bio">
    <div class="bio-info">
      <header>
        <div class="headshot">
          <img src="{{ URL::to('img/headshot-wendycarlos.jpg') }}" title="">
        </div>
        <h3>Dr. Wendy Carlos</h3>
        <h4>Trained Music Therapist and Cassical Composer</h4>
        <p>Nulla vitae elit libero, a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas faucibus mollis interdum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
      </header>
      <h5>Contact</h5>
      <p><span>Address</span> 999 Upsidedowner Rd.<br/>
      <span class="bio-adress-line-2"></span>Silverpress, MA 02762</p>
      <p><span>Email</span> SardonicMe@wendyiswinning.us</p>
      <p><span>Phone</span> 1 + (098) 765 - 4321</p>
      <h5>Details</h5>
      <p>Dr. Wendy Carlos serves patients within a <strong>5 mile</strong> radius.</p>
      <p>Dr. Wendy Carlos <strong>does</strong> offer private sessions.</p>
    </div>
    <iframe class="bio-map" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAUl_fk5GscSUEguTS7V9RWh9309pG2wtI&q=Boston,+MA/" allowfullscreen></iframe>
  </div>
</div>

@endsection
