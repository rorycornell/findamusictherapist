@extends('layouts.app')

@section('content')


<div class="main-content-wrapper">
  <div class="about-container">
    <header>
      <h2>Music Therapy:<br/>
      <span>To Complete your signup please pay $39.99 with Stripe</span></h2>
    </header>
    <div>
      <p><form action="/yearly" method="post">
        {{csrf_field() }}
        <script src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                data-key="{{config('services.stripe.key')}}"
                data-amount="3999" data-zip-code="True" data-description="One year's subscription"></script>
      </form>
      </p>

  </div>
</div>

@endsection
