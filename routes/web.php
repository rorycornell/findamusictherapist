<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
$user = DB::table('users')->where('id', 1)->first();

    return view('welcome', ['user' => $user]);
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/locations', 'LocationsController@index');
Route::post('/locations', 'LocationsController@send_to_locations');

Route::get('/search', function () {
  $users = DB::table('users')->get();

    return view('locations_no_zip', ['users' => $users]);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('user/{id}', 'UserController@show');

Route::get('/edit', 'UserController@edit')->name('edit');
Route::post('/edit', 'UserController@saveedits');
Route::post('/editpic', 'UserController@update_avatar');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');


Route::get('/therapist/johncage', function () {
    return view('johncage');
});

Route::get('/therapist/wendycarlos', function () {
    return view('wendycarlos');
});

Route::get('/payment', function () {
    return view('payment');
});

Route::get('/monthly', function () {
    return view('monthly');
});
Route::post('/monthly', 'PaymentController@monthly');

Route::get('/yearly', function () {
    return view('yearly');
});
Route::post('/yearly', 'PaymentController@yearly');


Route::post('/payment', 'PaymentController@store');

Route::get('/therapist/laurieanderson', function () {
    return view('laurieanderson');
});
